# React Starter Kit 2018 MA

Marco Amodio // @Marticvs <marcoamodio@protonmail.com>

## Prerequisites

  - node.js Node 8.10+
  - yarn

## Installation

```bash
$ git clone https://gitlab.com/Marticvs/mamodio-react-starterkit.git
$ cd mamodio-react-starterkit
$ yarn install
```

## Running the project

```bash
$ yarn start # Start the development server (or `npm start`)
$ http://localhost:3000/
```

there are additional scripts at your disposal:

```bash
|`yarn <script>`    |Description|
|-------------------|-----------|
|`start`            |Serves your app at `localhost:3000`|
|`build`            |Builds the application to ./dist|
|`test`             |Runs unit tests with Jest.|
|`lint`             |[Lints]() the project for potential errors|
```

## Project Structure

soon! (:
