import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Grid } from './components/Grid';

const App = () => (
  <BrowserRouter>
    <div>
      <Route exact path="/" component={Grid} />
    </div>
  </BrowserRouter>
);

export default App;
