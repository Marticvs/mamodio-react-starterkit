import React from 'react';
import ReactDOM from 'react-dom';
import './styles/main.scss';
import App from './App';

// translation
import enAppStrings from './translations/app-en.json';
import itAppStrings from './translations/app-it.json';

ReactDOM.render(
  <App messages={{ en: enAppStrings, it: itAppStrings }} />,
  document.getElementById('root'),
);
